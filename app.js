const express = require('express');

const app = express();

app.use('/bin', express.static('./bin'));
app.use('/', express.static('./'));

app.listen(8080);
