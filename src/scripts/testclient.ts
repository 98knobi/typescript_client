import { Grid } from './lib/grid.js';
import { Input } from './lib/input.js';

import { ComplexInput } from './lib/complexInput.js';

const myGrid = new Grid();

myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);
myGrid.AddRow(['a', 'b', 'c', 1, 2, 3]);

const testinput = new Input(document.querySelector('body'), { type: 'text' });
testinput.Value = 'Hello World!';
console.log(testinput.Value);

customElements.define('complex-input', ComplexInput);

const testelem = (document.getElementById('test') as ComplexInput);
testelem.value = 'Hello World!';

console.log(testelem.value);