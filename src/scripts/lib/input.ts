import { DOMElement } from './dom.js';

export class Input extends DOMElement {
    constructor(domRoot: HTMLElement, attributes: object) {
        super(domRoot, 'input', attributes);
    }

    get Value() {
        return (this.dom as HTMLInputElement).value;
    }

    set Value(val) {
        (this.dom as HTMLInputElement).value = val;
    }
}