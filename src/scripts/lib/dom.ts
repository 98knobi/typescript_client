export class DOMElement {    
    private domRoot: HTMLElement;
    private type: string;
    private attributes: object;

    public dom: HTMLElement;

    constructor(domRoot: HTMLElement = document.querySelector('body'), type: string = 'div', attributes?: object) {
        this.domRoot = domRoot;
        this.type = type;
        this.attributes = attributes;

        this.dom = document.createElement(this.type);

        for (let k in this.attributes) {
            this.dom.setAttribute(k, this.attributes[k]);
        }

        this.domRoot.appendChild(this.dom);
    }

    public Destroy() {
        this.domRoot.removeChild(this.dom);
    }
}