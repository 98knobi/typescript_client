export class ComplexInput extends HTMLElement {
    private inputField: HTMLInputElement;

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const wrapper = document.createElement('div');
        wrapper.classList.add('wrapper');        

        this.inputField = document.createElement('input');

        if (this.hasAttribute('value')) {
            this.value = this.getAttribute('value');
        }

        shadow.appendChild(wrapper);
        wrapper.appendChild(this.inputField);
    }

    get value() {
        return this.inputField.value;
    }

    set value(val) {
        this.setAttribute('value', val);
        this.inputField.value = val;
    }
}