import { DOMElement } from './dom.js';

export class Grid extends DOMElement {        
    private rows: Array<Row> = new Array<Row>();

    private data: JSON;

    constructor(domRoot?: HTMLElement, attributes?: object) {
        super(domRoot, 'grid', attributes);
    }

    public get Data() {
        return this.data;
    }

    public set Data(value: JSON) {
        this.data = value;
    }    

    public AddRow(values: Array<any>): Row {
        const newRow = new Row(this, values);
        this.rows.push(newRow);        
        return newRow;
    }
}

export class Row extends DOMElement {
    private data: Array<any>;

    public set Data(value: Array<any>) {
        this.data = value;
        this.dom.innerHTML = '';

        this.data.forEach(element => {
            const col = document.createElement('column');
            col.innerText = element;
            this.dom.appendChild(col)
        });
    }

    constructor(parentGrid: Grid, data: Array<any>, attributes?: object) {
        super(parentGrid.dom, 'row', attributes);

        this.Data = data;
    }
}